import cv2
import sys
from syringe_main import cvt_HSV
from syringe_main import cvt_BGR
from syringe_main import cvt_RGB


class mouseParam:
    def __init__(self, input_img_name):
        # マウス入力用のパラメータ
        self.mouseEvent = {"x": None, "y": None, "event": None, "flags": None}
        # マウス入力の設定
        cv2.setMouseCallback(input_img_name, self.__CallBackFunc, None)

    # コールバック関数
    def __CallBackFunc(self, eventType, x, y, flags, userdata):

        self.mouseEvent["x"] = x
        self.mouseEvent["y"] = y
        self.mouseEvent["event"] = eventType
        self.mouseEvent["flags"] = flags

    # マウス入力用のパラメータを返すための関数
    def getData(self):
        return self.mouseEvent

    # マウスイベントを返す関数
    def getEvent(self):
        return self.mouseEvent["event"]

    # マウスフラグを返す関数
    def getFlags(self):
        return self.mouseEvent["flags"]

    # xの座標を返す関数
    def getX(self):
        return self.mouseEvent["x"]

    # yの座標を返す関数
    def getY(self):
        return self.mouseEvent["y"]

    # xとyの座標を返す関数
    def getPos(self):
        return self.mouseEvent["x"], self.mouseEvent["y"]


def cvt_color(img, x, y):
    ret = cvt_HSV(img, y, x)
    return ret


def args_check(args):
    if len(args) == 2:
        filename = args[1]
    else:
        print(f'引数の数を確認してください\nargs = {args}')
        sys.exit(0)
    return filename


if __name__ == "__main__":
    args = sys.argv
    # filename = args_check(args)
    # 入力画像
    read = cv2.imread("sample.png")

    try:
        print("色空間を選択してください...")
        print("BGR = 0, HSV = 1, RGB = 2")
        mode = int(input())
    except ValueError:
        print("ValueError : 色空間を正しく選択してください")
        exit(0)

    # 表示するWindow名
    window_name = "input window"
    # 画像の表示
    cv2.imshow(window_name, read)
    # コールバックの設定
    mouseData = mouseParam(window_name)

    if mode >= 0 and mode <= 2:
        while 1:
            cv2.waitKey(20)
            # 左クリックがあったら表示
            if mouseData.getEvent() == cv2.EVENT_LBUTTONDOWN:
                x = mouseData.getX()
                y = mouseData.getY()

                if mode == 0:  # BGR
                    ret = cvt_BGR(read, y, x)  # 配列で返す
                if mode == 1:  # HSV
                    ret = cvt_HSV(read, y, x)  # 配列で返す
                if mode == 2:
                    ret = cvt_RGB(read, y, x)  # 配列で返す
                # cv2.putText(read, ret, (0, read.shape[0]), cv2.FONT_HERSHEY_COMPLEX, 0.8, (0, 255, 0), lineType=cv2.LINE_AA)  # 色情報テキストの描画
                cv2.imshow(window_name, read)
            # 右クリックがあったら終了
            elif mouseData.getEvent() == cv2.EVENT_RBUTTONDOWN:
                print(read.shape)
                break
    else:
        print("色空間を正しく選択してください")
        print(mode)
        exit(0)

    cv2.destroyAllWindows()
    print("Finished")
