import cv2
import sys


def cvt_BGR(img, y, x):
    try:
        B = img[y, x][0]
        G = img[y, x][1]
        R = img[y, x][2]
    except IndexError:
        print("IndexError : 色情報を取得できませんでした")
        exit(1)
    ret = [B, G, R]
    print(f"BGR = {ret}")
    return ret


def cvt_HSV(img, y, x):
    hsv_img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    try:
        H = hsv_img[y, x][0]
        S = hsv_img[y, x][1]
        V = hsv_img[y, x][2]
    except IndexError:
        print("IndexError : 色情報を取得できませんでした")
        exit(1)
    ret = [H, S, V]
    print(f"HSV = {ret}")
    return ret


def cvt_RGB(img, y, x):
    try:
        B = img.item(1000, x, 0)
        G = img.item(y, x, 1)
        R = img.item(y, x, 2)
    except IndexError:
        print("IndexError : 色情報を取得できませんでした")
        exit(1)
    ret = [R, G, B]
    print(f"RGB = {ret}")
    return ret


def input_file(filename):
    # ファイルの読み込み
    img = cv2.imread(filename)
    print('in:' + filename)
    cv2.imshow("image,img")
    return img


def args_check(args):
    if len(args) == 2:
        filename = args[1]
    else:
        print(f'引数の数を確認してください\nargs = {args}')
        sys.exit(0)
    return filename


if __name__ == "__main__":
    args = sys.argv
    filename = args_check(args)
    img = input_file(filename)
    print('-------------------------------------')
    hsv_img = cvt_HSV(img, 0, 0)
